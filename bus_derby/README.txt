A Pen created at CodePen.io. You can find this one at https://codepen.io/matthewmain/pen/YJwoVy.

 ![](https://dzwonsemrish7.cloudfront.net/items/2m0U072g25250e0I3m19/ezgif.com-video-to-gif.gif)

A 3D vehicle derby video game rendered in three.js and run on a physi.js physics engine. UV maps designed in Adobe Illustrator; meshes created  in Blender and imported as glTF. 

Check out my [tutorial](https://codepen.io/matthewmain/post/how-to-import-a-3d-blender-object-into-a-three-js-project-as-a-gltf-file) on using complex 3D shapes like this.